let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

mix.setPublicPath('dist')
   .setResourceRoot('dist/assets/')
   .js('src/js/app.js', 'dist/js')
   .js('src/js/dropdowns.js', 'dist/js')
   .sass('src/sass/app.scss', 'dist/css')
   .sass('src/sass/dropdowns.scss', 'dist/css')
   .options({ processCssUrls: false });



