import Chart from 'chart.js';

var barChartData = {
    labels: ["user1", "user2", "user3", "user4", "user5", "user6", "user7", "user8", "user9", "user10"],
    datasets: [{
        label: 'Low Activity',
        backgroundColor: '#F44336',
        borderColor: '#F44336',
        data: [
            -28,
            -24,
            -24,
            -20,
            -18,
            -18,
            -18,
            -18,
            -18,
            -10
        ]
    }, {
        label: 'Normal Activity',
        backgroundColor: '#CFD8DC',
        borderColor: '#CFD8DC',
        data: [
            65,
            80,
            80,
            87,
            89,
            91,
            91,
            91,
            94,
            96
        ]
    }]

};

var lineChartData = {
    labels: ["May 15", "May 16", "May 17", "May 18", "May 19", "May 20", "May 21"],
    datasets: [{
        label: 'P Designers',
        borderColor: '#5A68C6',
        backgroundColor: '#5A68C6',
        data: [
            90,
            55,
            110,
            125,
            110,
            25,
            5
        ],
        lineTension: 0,
        fill: false,
        pointRadius: 0,
        borderWidth:2
    }, {
        label: 'P Managers',
        borderColor: '#F44336',
        backgroundColor: '#F44336',
        data: [
            65,
            125,
            140,
            145,
            137,
            48,
            22
        ],
        lineTension: 0,
        fill: false,
        pointRadius: 0,
        borderWidth:2
    }, {
        label: 'P Developers',
        borderColor: '#FFB200',
        backgroundColor: '#FFB200',
        data: [
            135,
            105,
            125,
            110,
            125,
            35,
            25
        ],
        lineTension: 0,
        fill: false,
        pointRadius: 0,
        borderWidth:2
    }, {
        label: 'Support',
        borderColor: '#51BE71',
        backgroundColor: '#51BE71',
        data: [
            115,
            75,
            90,
            93,
            50,
            20,
            5
        ],
        lineTension: 0,
        fill: false,
        pointRadius: 0,
        borderWidth:2
    }, {
        label: 'Translation',
        borderColor: '#00D2ED',
        backgroundColor: '#00D2ED',
        data: [
            40,
            40,
            70,
            30,
            67,
            1,
            4
        ],
        lineTension: 0,
        fill: false,
        pointRadius: 0,
        borderWidth:2
    }]

};

window.onload = function() {
    var bar = document.getElementById("barChart").getContext("2d");
    window.myBar = new Chart(bar, {
        type: 'bar',
        data: barChartData,
        options: {
            title:{
                display:false,
                text:"Chart.js Bar Chart - Stacked"
            },
            legend: {
                fullWidth: true,
                labels: {
                    boxWidth: 8,
                    fontSize: 10,
                    padding: 13,
                    fontColor: '#77909D',

                },
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            responsive: true,
            barThickness: 5,
            scales: {
                xAxes: [{
                    stacked: true,
                    barThickness: 24,
                    gridLines: {
                        display: false,
                        offsetGridLines: false,
                        zeroLineWidth: 0,
                        drawBorder: false,
                        drawTicks: false
                    },
                    ticks: {
                        display: false,
                    }
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        // Include a dollar sign in the ticks
                        callback: function (value, index, values) {
                            return value + '%';
                        },
                        fontSize: 10,
                        fontColor: '#77909D',
                        max: 100,
                        stepSize: 50,
                        min: -100,
                        padding: 10

                    },
                    gridLines: {
                        borderDash: [5, 3],
                        zeroLineBorderDash: [5, 3],
                        offsetGridLines: false,
                        zeroLineWidth: 0,
                        drawBorder: false,
                        drawTicks: false
                    }
                }]
            }
        }
    });

    var line = document.getElementById("lineChart").getContext("2d");
    window.myLine = new Chart(line, {
        type: 'line',
        data: lineChartData,
        options: {
            responsive: true,
            title:{
                display:false,
                text:'Chart.js Line Chart'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            legend: {
                fullWidth: true,
                labels: {
                    boxWidth: 8,
                    fontSize: 10,
                    padding: 13,
                    fontColor: '#77909D',

                },
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    ticks: {
                        fontSize: 10,
                        fontColor: '#77909D',
                        padding: 20
                    },
                    gridLines: {
                        display: false,
                        offsetGridLines: false,
                        zeroLineWidth: 0,
                        drawBorder: false,
                        drawTicks: false
                    },
                }],
                yAxes: [{
                    display: true,
                    ticks: {
                        // Include a dollar sign in the ticks
                        callback: function (value, index, values) {
                            return value + 'h';
                        },
                        fontSize: 10,
                        fontColor: '#77909D',
                        max: 200,
                        stepSize: 50,
                        min: 0,
                        padding: 10
                    },
                    gridLines: {
                        borderDash: [5, 3],
                        zeroLineBorderDash: [5, 3],
                        offsetGridLines: false,
                        zeroLineWidth: 0,
                        drawBorder: false,
                        drawTicks: false
                    },
                }]
            }
        }
    });
};



