# TimeDoctor Test

#### **Pure HTML pixel perfect representation of provided design.**

## Introduction
This sample was built off the design found at:  https://projects.invisionapp.com/d/main#/console/10986713/266663454/preview.

The live version is at:  https://timedoctortest.000webhostapp.com/

I have made sure that it is absolutely pixel perfect based on this design. This project is made in pure HTML and Sass. Intitially, I planned on doing it 3 different ways. One in pure Html, then one Bootstrap 4 + VueJS, then one in Bootstrap 4 + ReactJS. Towards the end of this HMTL only one is when I realised that might have been abit too ambitious and that I would not be able to complete all I had hoped to.

My focus is however to ensure that all my designs are accurate to the pixel, whether I am using a specific framework or not (as is in this case). I am however well versed in ReactJs and VueJs.

I did not use a front end design framework (which would have been faster) because I wanted the design to be absolutely accurate without having to do some unconventional hacking. Also, most frameworks are quite easy to use and do most things for you. Because of this, many people are able to just add classes to divs and produce front end designs without really understanding how it happens behind the scenes. Since this is a test, I wanted to take the no framework route to show that I am actually a developer that understands thoroughly how to design a pixel perfect page from scratch. **Whatever the environment! :)**


----------
### Features
- Widgets were designed very robustly, and adding new ones is as easy as adding classes to divs. eg.

    ```
    <div class="widget one-third">
        <div class="header">
        </div>
        <div class="body">
        </div>
        <div class="footer">
        </div>
    </div>
    ```
    
- **Keyboard & Mouse Activity Widget** has been completed and is accurate according to the design.
	- The amount of users is the same as on the design.
	- The percentage is the same as presented on the design.
	- The monthly dropdown works and shows options of different time periods.
	- The vertical dots by the dropdown also work and show the exact menu and interface from the supplied design.
- **Comparing Hours Tracked Widget** has been completed and is accurate according to the design.
	- Data is the same as the design.
	- Working behaviour is the same as the above widget.

#### a) How I executed the task
- I used  only HTML and Sass to design the website from scratch, no frameworks.
- The reason I did this in only HTML was to exhibit how comfortable I am translating a design to pure HTML with pixel perfect accuracy.
- For the styling, I used Sass, and compiled it to css with webpack while I was working.

#### b) What libraries I used
- Chart.js for the bar and line chart.
- JQuery for the dropdowns functionality.
- Font awesome for icons.

#### c) Issues I faced
- Some of the icons used in the design were not provided in the assets supplied, so I had to use different but similar-looking ones.
- The legends on Chart.js are not fully customizable, so I was not able to float them to the top left of the graph as outlined in the design.

#### d) Any additional development
- Although I built it using raw HTML, I followed a convention that makes it similar to a framework.
- The page structure is very organized and easy to understand so that further development by other developers will not be difficult.
- When creating elements, the language is very intuitive eg.

    ```
    <div class="widget one-third">
        <div class="header">
        </div>
        <div class="body">
        </div>
        <div class="footer">
        </div>
    </div>
    ```
